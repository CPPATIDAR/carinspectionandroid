package com.acxiom.inspection.retrofit;

import com.acxiom.inspection.bean.CarMakeData;
import com.acxiom.inspection.bean.CarModelData;
import com.acxiom.inspection.bean.LoginData;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    //Add only Get type web service
    @GET("getcarmakes")
    Call<LoginData> getCarMake(@Header("Authorization") String header);


    //Add only Post type web service
    @FormUrlEncoded
    @POST("register")
    Call<LoginData> registerUser(@Field("email") String email,
                                 @Field("name") String name);

    @FormUrlEncoded
    @POST("login")
    Call<LoginData> loginUser(@Field("email") String email,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("mobile_make") String mobile_make,
                              @Field("mobile_model") String mobile_model,
                              @Field("android_version") String android_version,
                              @Field("app_version") String app_version);

    @FormUrlEncoded
    @POST("forgetPassword")
    Call<LoginData> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("updatePassword")
    Call<LoginData> forgotUpdatePassword(@Field("user_id") String user_id,
                                         @Field("forget_token") String forget_token,
                                         @Field("password") String password,
                                         @Field("password_confirmation") String password_confirmation);


    @Multipart
    @POST("savecarinspection")
    Call<LoginData> uploadData(@Header("Authorization") String header,
                                   @Part("car_model_id") RequestBody car_model_id ,
                                   @Part("car_type_id") RequestBody car_type_id,
                                   @Part("car_number") RequestBody car_number,

                                   @Part List<MultipartBody.Part> file,

                                   @Part("front_image_side") RequestBody front_image_side,
                                   @Part("front_lat") RequestBody front_lat,
                                   @Part("front_lng") RequestBody front_lng,
                                   @Part("front_time") RequestBody front_time_duration,
                                   @Part("front_steps") RequestBody front_steps_taken,
                                   @Part("front_camera_distance") RequestBody front_camera_distance,

                                   @Part("left_image_side") RequestBody left_image_side,
                                   @Part("left_lat") RequestBody left_lat,
                                   @Part("left_lng") RequestBody left_lng,
                                   @Part("left_time") RequestBody left_time_duration,
                                   @Part("left_steps") RequestBody left_steps_taken,
                                   @Part("left_camera_distance") RequestBody left_camera_distance,

                                   @Part("right_image_side") RequestBody right_image_side,
                                   @Part("right_lat") RequestBody right_lat,
                                   @Part("right_lng") RequestBody right_lng,
                                   @Part("right_time") RequestBody right_time_duration,
                                   @Part("right_steps") RequestBody right_steps_taken,
                                   @Part("right_camera_distance") RequestBody right_camera_distance,

                                   @Part("back_image_side") RequestBody back_image_side,
                                   @Part("back_lat") RequestBody back_lat,
                                   @Part("back_lng") RequestBody back_lng,
                                   @Part("back_time") RequestBody back_time_duration,
                                   @Part("back_steps") RequestBody back_steps_taken,
                                   @Part("back_camera_distance") RequestBody back_camera_distance);



    @Multipart
    @POST("updateProfile")
    Call<LoginData> updateProfile(@Header("Authorization") String header,
                                  @Part("name") RequestBody name,
                                  @Part("age") RequestBody age,
                                  @Part("relationship_status") RequestBody relationship_status,
                                  @Part("favorite_soca_genre") RequestBody favorite_soca_genre,
                                  @Part("country_id") RequestBody country_id,
                                  @Part("email") RequestBody email,
                                  @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("getcarmodels")
    Call<CarModelData> getCarModel(@Header("Authorization") String header,
                                   @Field("car_make_id") String car_make_id);


    @FormUrlEncoded
    @POST("createPlayList")
    Call<LoginData> createUserPlayList(@Header("Authorization") String header,
                                       @Field("title") String title);

    @FormUrlEncoded
    @POST("getUserPlaylists")
    Call<LoginData> getUserPlayList(@Header("Authorization") String header,
                                    @Field("value") String value);

    @FormUrlEncoded
    @POST("addToPlaylist")
    Call<LoginData> addToPlayList(@Header("Authorization") String header,
                                  @Field("playlist_id") String playlist_id,
                                  @Field("media_id") String media_id);

    @FormUrlEncoded
    @POST("getPlaylistMedias")
    Call<LoginData> getPlayListMedia(@Header("Authorization") String header,
                                     @Field("playlist_id") String playlist_id,
                                     @Field("media_type") String media_type,
                                     @Field("page") String page);

    @FormUrlEncoded
    @POST("updatePlayListName")
    Call<LoginData> updateUserPlayList(@Header("Authorization") String header,
                                       @Field("playlist_id") String playlist_id,
                                       @Field("title") String title);

    @FormUrlEncoded
    @POST("addToFavorite")
    Call<LoginData> addToFavorite(@Header("Authorization") String header,
                                  @Field("media_id") String media_id);

    @FormUrlEncoded
    @POST("getMedia")
    Call<LoginData> getMedia(@Header("Authorization") String header,
                             @Field("media_type") String media_type,
                             @Field("is_favorite") String is_favorite,
                             @Field("artist_id") String artist_id,
                             @Field("page") String page);

    @FormUrlEncoded
    @POST("getMedia")
    Call<LoginData> getVideoMedia(@Header("Authorization") String header,
                                  @Field("media_type") String media_type,
                                  @Field("is_favorite") String is_favorite,
                                  @Field("artist_id") String artist_id,
                                  @Field("page") String page);

    @FormUrlEncoded
    @POST("search")
    Call<LoginData> getSearchResult(@Header("Authorization") String header,
                                    @Field("search_keyword") String search_keyword,
                                    @Field("page") String page);

}
