package com.acxiom.inspection.activity;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.acxiom.inspection.R;
import com.acxiom.inspection.utils.ConstantMember;


public class SplashActivity extends Activity {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ConstantMember.getBooleanFromSession(getApplicationContext(), "is_login")) {
                    startActivity(new Intent(SplashActivity.this,HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                    finish();
                }
            }
        },3000);

    }
}
