package com.acxiom.inspection.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.acxiom.inspection.R;
import com.acxiom.inspection.bean.LoginData;
import com.acxiom.inspection.retrofit.Api;
import com.acxiom.inspection.retrofit.ApiBase;
import com.acxiom.inspection.utils.ConstantMember;
import com.acxiom.inspection.utils.NetworkConnection;
import com.acxiom.inspection.utils.SessionData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

public class InspectionCompleteActivity extends AppCompatActivity implements View.OnClickListener{

    Button submit;
    Context context;
    List<SessionData> dataList = new ArrayList<>();
    String strList = "",front_encode_image;
    String header,strCarModleId,strCarMakeId,strCarNo,strCarType;
    Gson gson = new Gson();
    ProgressDialog dialog;

    MultipartBody.Part bodyfiles = null;
    ArrayList<MultipartBody.Part> parts = new ArrayList<>();

    RequestBody front_side,front_lat,front_lng,front_time_duration,front_steps_taken,front_distance;
    RequestBody left_side,left_lat,left_lng,left_time_duration,left_steps_taken,left_distance;
    RequestBody right_side,right_lat,right_lng,right_time_duration,right_steps_taken,right_distance;
    RequestBody back_side,back_lat,back_lng,back_time_duration,back_steps_taken,back_distance;

    TextView tv_welcome_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_complete);
        context = InspectionCompleteActivity.this;
        header = ConstantMember.getHeader(context);
        initComponent();

        //Get other deatails
        strCarNo = ConstantMember.getValueFromSession(context,"car_number");
        strCarMakeId = ConstantMember.getValueFromSession(context,"car_make_id");
        strCarModleId = ConstantMember.getValueFromSession(context,"car_model_id");
        strCarType = ConstantMember.getValueFromSession(context,"car_type");
    }

    public void showDialog() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public RequestBody getBody(String value) {
        Log.e("body ","details= "+value);
        if (value == null) {
            value = "";
        }
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    public void clickOnSubmit() throws JSONException {
        strList = ConstantMember.getValueFromSession(context, "list_inspection");
        if (!strList.equalsIgnoreCase("")) {
            dataList = gson.fromJson(strList,new TypeToken<List<SessionData>>(){}.getType());
            if (dataList.size() > 0) {
                for (int i = 0; i < dataList.size(); i++){
                    Log.e("image path","side= "+dataList.get(i).getSide());
                    if (dataList.get(i).getSide().equals("front")) {
                        front_side = getBody(dataList.get(i).getSide());
                        front_lat = getBody(dataList.get(i).getLat());
                        front_lng = getBody(dataList.get(i).getLang());
                        front_time_duration = getBody(dataList.get(i).getCapture_time());
                        front_steps_taken = getBody(dataList.get(i).getSteps_taken());
                        front_distance = getBody(dataList.get(i).getDistance());
//                        front_encode_image = getEncoded64ImageStringFromBitmap(dataList.get(i).getFile_path());
//                        uploadBase64Image(front_encode_image);
                    }

                    if (dataList.get(i).getSide().equals("left")) {
                        left_side = getBody(dataList.get(i).getSide());
                        left_lat = getBody(dataList.get(i).getLat());
                        left_lng = getBody(dataList.get(i).getLang());
                        left_time_duration = getBody(dataList.get(i).getCapture_time());
                        left_steps_taken = getBody(dataList.get(i).getSteps_taken());
                        left_distance = getBody(dataList.get(i).getDistance());
                    }
////
                    if (dataList.get(i).getSide().equals("right")) {
                        right_side = getBody(dataList.get(i).getSide());
                        right_lat = getBody(dataList.get(i).getLat());
                        right_lng = getBody(dataList.get(i).getLang());
                        right_time_duration = getBody(dataList.get(i).getCapture_time());
                        right_steps_taken = getBody(dataList.get(i).getSteps_taken());
                        right_distance = getBody(dataList.get(i).getDistance());
                    }
////
                    if (dataList.get(i).getSide().equals("back")) {
                        back_side = getBody(dataList.get(i).getSide());
                        back_lat = getBody(dataList.get(i).getLat());
                        back_lng = getBody(dataList.get(i).getLang());
                        back_time_duration = getBody(dataList.get(i).getCapture_time());
                        back_steps_taken = getBody(dataList.get(i).getSteps_taken());
                        back_distance = getBody(dataList.get(i).getDistance());
                    }

                    File file = new File(dataList.get(i).getFile_path());
                    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                    bodyfiles = MultipartBody.Part.createFormData("images_data"+dataList.get(i).getSide(),"image_"+dataList.get(i).getSide()+".jpg", reqFile);
                    parts.add(bodyfiles);
                }
                Log.e("image path","multipar size= "+parts.size());
                apiCallUploadData();
            }
        }
    }

    public void apiCallUploadData() {
        showDialog();
        RequestBody bodyCarModleId = RequestBody.create(MediaType.parse("text/plain"), strCarModleId);
        RequestBody bodyCarType = RequestBody.create(MediaType.parse("text/plain"), strCarType);
        RequestBody bodyCarNo = RequestBody.create(MediaType.parse("text/plain"), strCarNo);
        Api.getClient().uploadData(header,bodyCarModleId,bodyCarType,bodyCarNo,parts,
                front_side,front_lat,front_lng,front_time_duration,front_steps_taken,front_distance,
                left_side,left_lat,left_lng,left_time_duration,left_steps_taken,left_distance,
                right_side,right_lat,right_lng,right_time_duration,right_steps_taken,right_distance,
                back_side,back_lat,back_lng,back_time_duration,back_steps_taken,back_distance)
                .enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
              Log.e("upload","is successfull= "+response+"\n"+response.body().isStatus());
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
//                        ConstantMember.showMessage(context,""+response.body().getMessage());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ConstantMember.setBooleanValueInSession(context,"is_save",true);
                                dialog.dismiss();
                                confirmDialog();
                            }
                        },1000);

                    } else {
                        ConstantMember.showMessage(context,""+response.body().getMessage());
                        dialog.dismiss();
                    }
                } else {
                    ConstantMember.showMessage(context,"Something went wrong");
                    dialog.dismiss();
                }
              dialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                dialog.dismiss();
                ConstantMember.showMessage(context,"Something went wrong");
               Log.e("upload error","img= "+t);
            }
        });
    }

    private void confirmDialog() {
        new AlertDialog.Builder(InspectionCompleteActivity.this)
                .setTitle("Inspection Complete")
                .setMessage("Your inspection details have been submitted. Thank you")
                .setIcon(R.drawable.app_icon)
                .setCancelable(false)
                .setNegativeButton("CLOSE APP", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();
                    }
                })
                .setPositiveButton("NEW INSPECTION", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        startActivity(new Intent(context,HomeActivity.class));
                        finish();
                    }
                }).show();
    }

    public void initComponent() {

        tv_welcome_user = findViewById(R.id.tv_welcome_user);
        tv_welcome_user.setText("WELCOME  "+ConstantMember.getValueFromSession(getApplicationContext(),"name"));

        submit = findViewById(R.id.button_sumbit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_sumbit:
                if (NetworkConnection.checkConnection(context)) {
                    try {
                        clickOnSubmit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
                default:break;
        }
    }

    public void uploadBase64Image(String encodeImage) throws JSONException {

        JSONObject object = new JSONObject();
        object.put("image",encodeImage);
        Log.e("encode ","json object =  "+object);
        ApiBase.getClient().uploadImage(object).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e("encode ","upload image = "+response.isSuccessful()+"\n"+response.body());
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e("encode ","error =  "+t);
            }
        });
    }

    //Get base 64 image
//    Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
    public String getEncoded64ImageStringFromBitmap(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] byteFormat = stream.toByteArray();
            // get the base 64 string
            String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
            return imgString;
        }
        return "";
    }
}
