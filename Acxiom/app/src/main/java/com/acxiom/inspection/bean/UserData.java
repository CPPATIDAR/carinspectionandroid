package com.acxiom.inspection.bean;

import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("id")
    private int  id;

    @SerializedName("name")
    private String  name;

    @SerializedName("email")
    private String  email;

    @SerializedName("signIn_date")
    private String  signIn_date;

    @SerializedName("profile_picture")
    private String  profile_picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSignIn_date() {
        return signIn_date;
    }

    public void setSignIn_date(String signIn_date) {
        this.signIn_date = signIn_date;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}
