package com.acxiom.inspection.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.acxiom.inspection.R;
import com.acxiom.inspection.bean.LoginData;
import com.acxiom.inspection.bean.UserData;
import com.acxiom.inspection.retrofit.Api;
import com.acxiom.inspection.utils.ConstantMember;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

//    testuser@car.com
//    123456
    Button submit;
    TextInputEditText edittext_username, edittext_password;
    String  username, password;
    String app_version = "not available",device_token,mobile_make, mobile_model,android_version;
    Context context;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context  = LoginActivity.this;
        initComponent();
        initListener();
        //For check runtime permission
        ConstantMember.checkPermission(LoginActivity.this);

    }

    private void initComponent() {
        submit = findViewById(R.id.btn_login);
        edittext_username = findViewById(R.id.et_username);
        edittext_password = findViewById(R.id.et_password);
    }

    private void initListener() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (ConstantMember.checkPermission(LoginActivity.this)) {
                        callApi();
                    }
                }
            }
        });
    }

    public void getAppInfor() {
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            app_version = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {

        boolean flag = true;
        username = "" + edittext_username.getText();
        password = "" + edittext_password.getText();

        if (username.length() == 0) {
            edittext_username.setError("Please enter the username");
            flag = false;
        }

        if (password.length() == 0) {
            edittext_password.setError("Please enter the password");
            flag = false;
        }
        return flag;
    }

    public void callApi() {
        showDialog();
        Api.getClient().loginUser(username,password,device_token,mobile_make,mobile_model,android_version,app_version).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, final Response<LoginData> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        showToast(response.body().getMessage());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                ConstantMember.setValueInSession(getApplicationContext(), "access_token", response.body().getAccess_token());
                                ConstantMember.setValueInSession(getApplicationContext(), "token_type", response.body().getToken_type());
                                navigate(response.body().getUserData());
                            }
                        }, 2000);
                    } else {
                        dialog.dismiss();
                        ConstantMember.showMessage(context, "" + response.body().getMessage());
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginData> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e("login","error= "+t);
            }
        });
    }

    public void navigate(UserData userData) {
        ConstantMember.setValueInSession(getApplicationContext(),"user_id",""+userData.getId());
        ConstantMember.setValueInSession(getApplicationContext(),"name",userData.getName());
        ConstantMember.setValueInSession(getApplicationContext(),"email",userData.getEmail());
        ConstantMember.setValueInSession(getApplicationContext(),"profile_pic",userData.getProfile_picture());
        ConstantMember.setBooleanValueInSession(context,"is_login",true);

        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }

    public void showDialog() {
        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void showToast(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

}
