package com.acxiom.inspection.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.acxiom.inspection.R;
import com.acxiom.inspection.utils.ConstantMember;
import com.acxiom.inspection.utils.SessionData;

import java.util.ArrayList;
import java.util.List;

public class InspectionActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView image_front,image_right,image_back,image_left;
    Button button_start;
    TextView textView_welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection);
        initComponent();
        initListener();
    }

    private void initComponent() {
        textView_welcome=findViewById(R.id.tv_welcome_user);
        textView_welcome.setText("WELCOME  "+ConstantMember.getValueFromSession(getApplicationContext(),"name"));
        button_start=findViewById(R.id.button_start);
        button_start.setOnClickListener(this);
    }

    private void initListener() {

    }

    @Override
    public void onClick(View v) {
        Log.e("permission","check = "+ConstantMember.checkPermission(InspectionActivity.this));
        if (ConstantMember.checkPermission(InspectionActivity.this)) {
            startActivity(new Intent(this, CameraActivity.class));
            finish();
        }
    }
}
