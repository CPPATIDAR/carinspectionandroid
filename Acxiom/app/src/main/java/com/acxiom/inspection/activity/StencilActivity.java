package com.acxiom.inspection.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.acxiom.inspection.R;
import com.acxiom.inspection.utils.ConstantMember;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class StencilActivity extends AppCompatActivity implements View.OnClickListener{

    Context context ;
    TextView textView_welcome;
    private int issubscribed=0;
    private long total;
    private static final int RC_SIGN_IN = 0;
    private static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stencil);
        context = StencilActivity.this;
        initcomponents();
//        loginWithGoogle();
        initSteps();
    }

    private void loginWithGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Log.e("Step1 ","client "+mGoogleSignInClient);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                signIn();
            }
        },2000);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        Log.e("Step2 ","signin intent "+signInIntent);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                handleSignInResult(task);
            } catch (ApiException e) {
                Log.e("Step6","Exception "+e);
                e.printStackTrace();
            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) throws ApiException {

        Log.e("Step3  ","result "+" data "+completedTask);
        GoogleSignInAccount account = completedTask.getResult(ApiException.class);
        String idToken = account.getIdToken();
        Log.e("Step4  ","result data "+account);
       String gMail=account.getEmail();
//        txtGoogleFirstName=account.getGivenName();
//        txtGoogleLastName=account.getFamilyName();

        Log.e("Step5","login "+gMail);
        initSteps();
    }

    public void initSteps() {
                FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .build();
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), fitnessOptions)) {
            Log.e("Step0","fit option ");
            GoogleSignIn.requestPermissions(this,REQUEST_OAUTH_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(context),
                    fitnessOptions);
        } else {
            Log.e("Step6","fit option ");
            subscribe();
        }
    }

    public void subscribe() {
        // To create a subscription, invoke the Recording API. As soon as the subscription is
        Log.e("Step7","fit option ");
        Fitness.getRecordingClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    issubscribed=1;
                                    Log.e("Step8","fit option ");
                                   Timer myTimer = new Timer();
                                    myTimer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            if(issubscribed==1) {

                                                readData();

                                            }
                                        }

                                    }, 0, 2000);

                                } else {
                                    Log.e("Step9","fit option "+task.getException());
                                }
                            }
                        });
    }

    private void readData() {

        Fitness.getHistoryClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener(
                        new OnSuccessListener<DataSet>() {
                            @Override
                            public void onSuccess(DataSet dataSet) {
                                total = dataSet.isEmpty()
                                        ? 0
                                        : dataSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                                Log.e("Step10","fit steps "+total);
                            }
                        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "There was a problem getting the step count.", e);
                    }
                });
    }

    private void initcomponents() {
        textView_welcome=findViewById(R.id.tv_welcomeuser);
        textView_welcome.setText("WELCOME  "+ ConstantMember.getValueFromSession(getApplicationContext(),"name"));
    }

    @Override
    public void onClick(View v) {

    }
}
