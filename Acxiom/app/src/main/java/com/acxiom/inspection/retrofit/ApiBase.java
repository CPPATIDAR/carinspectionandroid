package com.acxiom.inspection.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBase {

    private static Retrofit retrofit = null;
    public static ApiBaseInterface getClient() {
        String BASE_URL = "http://18.235.0.101:8002/";//predict
        // change your base URL
        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        //Creating object for our interface
        return retrofit.create(ApiBaseInterface.class); // return the APIInterface object
    }
}
