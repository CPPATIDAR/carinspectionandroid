package com.acxiom.inspection.retrofit;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    private static Retrofit retrofit = null;
    public static ApiInterface getClient() {
         String BASE_URL = "https://laravel.cppatidar.com/car_inspection/public/api/auth/";
        // change your base URL
        if (retrofit==null) {

//            OkHttpClient client = new OkHttpClient.Builder()
//                    .connectTimeout(60, TimeUnit.SECONDS)
//                    .readTimeout(60, TimeUnit.SECONDS).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        //Creating object for our interface
        return retrofit.create(ApiInterface.class); // return the APIInterface object
    }
}
