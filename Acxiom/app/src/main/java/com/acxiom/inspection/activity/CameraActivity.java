package com.acxiom.inspection.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acxiom.inspection.R;
import com.acxiom.inspection.utils.ConstantMember;
import com.acxiom.inspection.utils.SessionData;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CameraActivity extends AppCompatActivity implements SurfaceHolder.Callback , View.OnClickListener {
    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    LinearLayout button_layout;
    Camera.PictureCallback jpegCallback;
    Button button_confirm,button_cancel,button_capture;
    Date curentTime;
    Bitmap finalImage;
    ImageView frame_image,iv_hint_portrait,iv_hint_landscape,iv_hint_landscap_left;
//    SessionData sessionData=new SessionData(",","","","","","");
    List<SessionData> inspection_complete_data;
//    int arrFrames[] = {R.drawable.frame_front,R.drawable.frame_left,R.drawable.frame_back,R.drawable.frame_right};
//    int frameResourceId = arrFrames[0];
    int[] arrFrames;
    int frameResourceId;

    int totalSteps = 0;
    String type = "front",strCameraDistance;

    double new_Latitude, new_longitude;
    LocationListener listener;
    LocationManager lm;

    Context context;
    String strCarTupe,dialogMessage;
    Handler handler = new Handler();
    TextView heading_tv,tv_welcome_user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        context = CameraActivity.this;

        iv_hint_landscape = findViewById(R.id.img_landscape);
        iv_hint_portrait  = findViewById(R.id.img_portrait);
        iv_hint_landscap_left = findViewById(R.id.img_landscape_right);

        tv_welcome_user = findViewById(R.id.tv_welcome_user);
        tv_welcome_user.setText("WELCOME  "+ConstantMember.getValueFromSession(getApplicationContext(),"name"));

        heading_tv = findViewById(R.id.heading);
        heading_tv.setText("Car Front Image");
        dialogMessage=("Car Front Image will be uploaded, kindly reconfirm to proceed\n"
                + "\nKindly tilt your phone Right for the next picture");

        surfaceView = findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();
        button_cancel=findViewById(R.id.button_cancel);
        button_cancel.setOnClickListener(this);
        button_confirm=findViewById(R.id.button_confirm);
        button_confirm.setOnClickListener(this);

        button_layout=findViewById(R.id.linear);
        surfaceHolder.addCallback(this);
        frame_image = findViewById(R.id.imageview);

        button_capture=findViewById(R.id.capture);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        //Select the frames
        strCarTupe = ConstantMember.getValueFromSession(context,"car_type");
        if (!strCarTupe.equalsIgnoreCase("")) {
            if (strCarTupe.equalsIgnoreCase("SUV")) {
                arrFrames = new int[]{R.drawable.frame_front, R.drawable.frame_left, R.drawable.frame_back, R.drawable.frame_right};
            }else if (strCarTupe.equalsIgnoreCase("Sedan")) {
                arrFrames = new int[]{R.drawable.sedan_front, R.drawable.sedan_left, R.drawable.sedan_back, R.drawable.sedan_right};
            } else if (strCarTupe.equalsIgnoreCase("Hatchback")) {
                arrFrames = new int[]{R.drawable.hatchback_front, R.drawable.hatchback_left, R.drawable.hatchback_back, R.drawable.hatchback_right};
            } else {
                //default frames for load suv type
                arrFrames = new int[]{R.drawable.frame_front, R.drawable.frame_left, R.drawable.frame_back, R.drawable.frame_right};
            }
        } else {
            arrFrames = new int[]{R.drawable.frame_front, R.drawable.frame_left, R.drawable.frame_back, R.drawable.frame_right};
        }

        surfaceView.setBackground(getResources().getDrawable(arrFrames[0]));
        frameResourceId = arrFrames[0];
//        dialogMessage=("Do you really want to Confirm?\n" + "Once you Confirmed \n" +
//                "it cannot be changed\n" + "Rotate your device to RIGHT");

        //Get lat lng initial
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLatLong();

        inspection_complete_data= new ArrayList<>();

        jpegCallback = new Camera.PictureCallback() {

            public void onPictureTaken(byte[] data, Camera camera) {

                try {
                    Bitmap myBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Bitmap overlay = BitmapFactory.decodeResource(getResources(),frameResourceId);

                    finalImage = Bitmap.createBitmap(myBitmap.getWidth(), myBitmap.getHeight(), myBitmap.getConfig());

                    Canvas canvas = new Canvas(finalImage);
                    canvas.drawBitmap(myBitmap, null, new Rect(0, 0,myBitmap.getWidth(), myBitmap.getHeight()), new Paint());
                    canvas.drawBitmap(overlay, null, new Rect(0, 0,  myBitmap.getWidth(), myBitmap.getHeight()), new Paint());

                    surfaceView.setVisibility(View.GONE);
                    button_capture.setVisibility(View.GONE);
                    button_layout.setVisibility(View.VISIBLE);
                    frame_image.setVisibility(View.VISIBLE);
                    frame_image.setImageBitmap(finalImage);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

       // setCamFocusMode();

        findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    captureImage(view);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void captureImage(View v) throws IOException {
        //take the picture
        try{
            if (camera != null) {
                camera.startPreview();
                strCameraDistance = getDistance();
                camera.takePicture(null, null, jpegCallback);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            System.err.println(e);
            return;
        }
        Camera.Parameters param;
        param = camera.getParameters();

        // modify parameter
        List<Camera.Size> sizes = param.getSupportedPreviewSizes();
        Camera.Size size = sizes.get(0);
        for(int i=0;i<sizes.size();i++)
        {
            if(sizes.get(i).width > size.width)
                size = sizes.get(i);
        }
        param.setPictureSize(size.width, size.height);
        param.setJpegQuality(100);
        param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        Display display =   ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int or = getWindowManager().getDefaultDisplay().getRotation();
        Log.e("check ","rotation= "+or);

        if(display.getRotation() == Surface.ROTATION_0){
            camera.setDisplayOrientation(90);
            or=90;
        }

        if(display.getRotation() == Surface.ROTATION_180){
            camera.setDisplayOrientation(270);
            or=270;
        }

        if(display.getRotation() == Surface.ROTATION_270){
            camera.setDisplayOrientation(180);
            or=180;
        }
        Log.e("check 2 ","rotation= "+or);
        param.setRotation(or);
        camera.setParameters(param);
        try {
            camera.setDisplayOrientation(90);
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.

         //refreshCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // stop preview and release camera
        camera.stopPreview();
        camera.release();
        camera = null;
    }

   public void refreshCamera() {
  /*      if (surfaceHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }*/

       button_layout.setVisibility(View.GONE);
       surfaceView.setVisibility(View.VISIBLE);
       button_capture.setVisibility(View.VISIBLE);


    }


    @Override
    public void onClick(final View v) {

        if (v == button_confirm) {

             readData();

            new AlertDialog.Builder(this)
                    .setTitle("Inspection Confirmation")
                    .setMessage(dialogMessage)
                    .setIcon(R.drawable.app_icon)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {


                            if (frameResourceId == arrFrames[0]) {
                                type = "front";
                                dialogMessage=("Car Left Image will be uploaded, kindly reconfirm to proceed\n"
                                        + "\nKindly tilt your phone Left for the next picture");
                                leftFrame(v);
                                iv_hint_landscape.setVisibility(View.VISIBLE);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        iv_hint_landscape.setVisibility(View.INVISIBLE);
                                    }
                                },3000);

                            } else if (frameResourceId == arrFrames[1]) {
                                type = "left";
                                dialogMessage=("Car Back Image will be uploaded, kindly reconfirm to proceed\n"
                                        + "\nKindly tilt your phone Left for the next picture");
                                backFrame(v);
                                iv_hint_portrait.setVisibility(View.VISIBLE);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        iv_hint_portrait.setVisibility(View.INVISIBLE);
                                    }
                                },3000);

                            } else if (frameResourceId == arrFrames[2]) {
                                type = "back";
                                dialogMessage=("Car Right Image will be uploaded\n"+"kindly reconfirm to proceed");
                                rightFrame(v);
                                iv_hint_landscap_left.setVisibility(View.VISIBLE);
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        iv_hint_landscap_left.setVisibility(View.INVISIBLE);
                                    }
                                },3000);

                            } else if (frameResourceId == arrFrames[3]) {
                                type = "right";
                                startActivity(new Intent(CameraActivity.this, InspectionCompleteActivity.class));
                                finish();

                            } else if (v == button_cancel) {
                                refreshCamera();
                            }


                            saveImage(finalImage);

                        }


                    })
                    .setNegativeButton(android.R.string.no, null).show();


        } else if (v == button_cancel) {

            refreshCamera();

        }

    }




    private void leftFrame(View v)  {
        heading_tv.setText("Car Left Image");
        surfaceView.setVisibility(View.VISIBLE);
        button_layout.setVisibility(View.GONE);
        button_capture.setVisibility(View.VISIBLE);
        surfaceView.setBackground(getResources().getDrawable(arrFrames[1]));
        frameResourceId = arrFrames[1];
    }

    private void backFrame(View v) {
        heading_tv.setText("Car Back Image");
        surfaceView.setVisibility(View.VISIBLE);
        button_layout.setVisibility(View.GONE);
        button_capture.setVisibility(View.VISIBLE);
        surfaceView.setBackground(getResources().getDrawable(arrFrames[2]));
        frameResourceId = arrFrames[2];
    }


    private void rightFrame(View v) {
        heading_tv.setText("Car Right Image");
        surfaceView.setVisibility(View.VISIBLE);
        button_layout.setVisibility(View.GONE);
        button_capture.setVisibility(View.VISIBLE);
        surfaceView.setBackground(getResources().getDrawable(arrFrames[3]));
        frameResourceId = arrFrames[3];
    }


    private void saveImage(Bitmap finalImage) {


        boolean success = true;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root +"/CarInspection");
        myDir.mkdirs();
        curentTime = new Date();
        String fname = "Image"+curentTime+".jpg";

        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
        String time = localDateFormat.format(new Date());

        SessionData data = new SessionData();
        data.setCapture_time(""+time);
        data.setSide(type);
        data.setFile_path(myDir+"/"+fname);
        data.setLat("" +ConstantMember.getValueFromSession(getApplicationContext(), "latitude"));
        data.setLang("" +ConstantMember.getValueFromSession(getApplicationContext(), "longitude"));
        data.setSteps_taken(""+totalSteps);
        data.setDistance(strCameraDistance);

        inspection_complete_data.add(data);


        Gson gson = new Gson();
        String strListData = gson.toJson(inspection_complete_data);
        ConstantMember.setValueInSession(getApplicationContext(),"list_inspection",strListData);


        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            if (finalImage != null) {
                FileOutputStream out = new FileOutputStream(file);
            finalImage.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (success) {
            Toast.makeText(getApplicationContext(), "Image saved with success",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Error during image saving", Toast.LENGTH_LONG).show();
        }
    }

    //Method lat long
    private void getLatLong() {

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if (location != null) {
                    DecimalFormat dFormat = new DecimalFormat("#.####");
                    new_Latitude = Double.valueOf(dFormat.format(location.getLatitude()));
                    new_longitude = Double.valueOf(dFormat.format(location.getLongitude()));

                    ConstantMember.setValueInSession(getApplicationContext(),"latitude",""+new_Latitude);
                    ConstantMember.setValueInSession(getApplicationContext(),"longitude",""+new_longitude);
                    lm.removeUpdates(listener);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return;
        }

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
    }

    private void readData() {

        Fitness.getHistoryClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener(
                        new OnSuccessListener<DataSet>() {
                            @Override
                            public void onSuccess(DataSet dataSet) {
                                totalSteps = dataSet.isEmpty() ? 0 : dataSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();
                                Log.e("Step10","fit steps "+totalSteps);
                            }
                        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "There was a problem getting the step count.", e);
                    }
                });
    }
    public String getDistance() {
        double inft, convert, value = 0.3048;
        int ft = 0;
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            float f[]=new float[3];
            parameters.getFocusDistances(f);

            String distance = ""+f[2];
            if (distance.equalsIgnoreCase("Infinity")) {
                ft = 0;
            }else {
                convert = f[2];
                inft = convert / value;
                ft = (int)inft;
            }
            Log.e("focus ","convert fit= "+f[1]+" after fit= "+ft+"\n\n\n");
        }
        return  ""+ft;
    }
}



