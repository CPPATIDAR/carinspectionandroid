package com.acxiom.inspection.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConstantMember {

    private static final int REQUEST_PERMISSION = 200;

  //Method for show toas message
  public static void showMessage(Context context,String message) {
      Toast.makeText(context,message,Toast.LENGTH_LONG).show();
  }

    public static boolean isValidEmail(String emailInput) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailInput);
        return !matcher.matches();
    }

    public static String getHeader(Context context){
       SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
           String token_type,access_token,headers;

            token_type = preferences.getString("token_type", "");
            access_token = preferences.getString("access_token", "");
            headers = token_type + " " + access_token;

      return headers;
    }

    public static void  setValueInSession(Context context,String key,String value){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(key,value).apply();
  }

  public static String getValueFromSession(Context context,String key){
      SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
      return preferences.getString(key,"");
  }

    public static void  setBooleanValueInSession(Context context,String key,boolean value){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(key,value).apply();
    }

    public static boolean getBooleanFromSession(Context context,String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key,false);
    }

    public static boolean checkPermission(Activity context) {
        boolean flag = true;

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED  ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED
                ){ ActivityCompat.requestPermissions(context, new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.READ_EXTERNAL_STORAGE,
                     Manifest.permission.CAMERA,
                     Manifest.permission.ACCESS_COARSE_LOCATION,
                     Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_PERMISSION);

                flag = false;
        }

        return  flag;
    }

}
