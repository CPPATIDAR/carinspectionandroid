package com.acxiom.inspection.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarModelData {

    @SerializedName("status")
    private boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("response")
    private ArrayList<CarModelListData> carModelList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CarModelListData> getCarModelList() {
        return carModelList;
    }

    public void setCarModelList(ArrayList<CarModelListData> carModelList) {
        this.carModelList = carModelList;
    }
}
