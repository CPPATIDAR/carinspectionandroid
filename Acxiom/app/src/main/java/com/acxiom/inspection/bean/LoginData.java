package com.acxiom.inspection.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoginData {

    @SerializedName("status")
    private boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("access_token")
    private String access_token;

    @SerializedName("token_type")
    private String token_type;

    @SerializedName("user")
    private UserData userData;

    @SerializedName("response")
    private ArrayList<CarMakeData> carMakeList;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public ArrayList<CarMakeData> getCarMakeList() {
        return carMakeList;
    }

    public void setCarMakeList(ArrayList<CarMakeData> carMakeList) {
        this.carMakeList = carMakeList;
    }
}
