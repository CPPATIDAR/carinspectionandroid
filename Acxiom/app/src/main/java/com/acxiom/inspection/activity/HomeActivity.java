package com.acxiom.inspection.activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Network;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.acxiom.inspection.R;
import com.acxiom.inspection.bean.CarMakeData;
import com.acxiom.inspection.bean.CarModelData;
import com.acxiom.inspection.bean.CarModelListData;
import com.acxiom.inspection.bean.LoginData;
import com.acxiom.inspection.retrofit.Api;
import com.acxiom.inspection.utils.ConstantMember;
import com.acxiom.inspection.utils.NetworkConnection;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Context context;
    Spinner spinner_make, spinner_model, spinner_type;
    Button button_next;
    EditText editText_carNumber,editTextCarType;
    String car_number,header;
    TextView textViewName;
    ProgressDialog dialog;

    int car_make_id = 0;
    String[] car_make;
    String[] car_model;

    List<CarMakeData> carMakeDataList = new ArrayList<>();
    List<CarModelListData> listCarModelList = new ArrayList<>();
    ArrayAdapter adapter_make;
    ArrayAdapter<String> adapter_model;
    Handler handler = new Handler();
    private int issubscribed=0;
    private static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;
        header = ConstantMember.getHeader(context);
        initcomponents();
        initListener();
        initSteps();
        ConstantMember.setValueInSession(context,"car_make_id","");
        ConstantMember.setValueInSession(context,"car_model_id","");
        if (NetworkConnection.checkConnection(context)) {
            if (ConstantMember.getBooleanFromSession(context, "is_save")) {
                if (NetworkConnection.checkConnection(context)) {
                    editText_carNumber.setText("");
                    editTextCarType.setText("");
                    spinner_model.setSelection(0);
                    ConstantMember.setValueInSession(context, "car_make_id", "");
                    ConstantMember.setValueInSession(context, "car_model_id", "");
                    Log.e("call first 2 ", "resume");
                    apiCallCarMake();
                    ConstantMember.setBooleanValueInSession(context, "is_save", false);
                }
            } else {
                Log.e("call first 1 ","resume");
                apiCallCarMake();
            }
        }
    }

    private void initcomponents() {
        spinner_make = findViewById(R.id.spinner_select_car_make);
        spinner_model = findViewById(R.id.spinner_select_car_model);
        spinner_type = findViewById(R.id.spinner_select_car_type);
        button_next=findViewById(R.id.button_next);
        editText_carNumber=findViewById(R.id.et_car_number);
        editTextCarType = findViewById(R.id.edit_car_type);
        textViewName = findViewById(R.id.tv_welcome);
        textViewName.setText("WELCOME  "+ConstantMember.getValueFromSession(getApplicationContext(),"name"));
    }

    private void initListener() {
        button_next.setOnClickListener(this);
    }

    public void apiCallCarMake() {
        showDialog();
        Api.getClient().getCarMake(header).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        dialog.dismiss();
                        carMakeDataList = response.body().getCarMakeList();
                        CarMakeData data = new CarMakeData();
                        data.setId(0);
                        data.setTitle("Select Car Make");
                        carMakeDataList.add(0,data);
                        if (carMakeDataList.size() > 0) {
                            int size = carMakeDataList.size();
                            car_make = new String[size];
                            for (int i = 0; i < car_make.length; i++) {
                                if (i == 0) {
                                    car_make[i] = "Select Car Make";
                                } else {
                                    car_make[i] = carMakeDataList.get(i).getTitle();
                                }
                            }
                            setSpinnerValues(car_make);
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                dialog.dismiss();
            }
        });
    }

    public void apiCallCarModel() {
        showDialog();
        Api.getClient().getCarModel(header,""+car_make_id).enqueue(new Callback<CarModelData>() {
            @Override
            public void onResponse(Call<CarModelData> call, Response<CarModelData> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        listCarModelList = response.body().getCarModelList();
                        CarModelListData data = new CarModelListData();
                        data.setId(0);
                        data.setTitle("Select Car Model");
                        data.setCar_type("Nothing");
                        listCarModelList.add(0,data);
                        if (listCarModelList.size() > 0) {
                            dialog.dismiss();
                            car_model = new String[listCarModelList.size()];
                            for (int i = 0; i<car_model.length; i++){
                                if (i == 0) {
                                    car_model[i] = "Select Car Model";
                                } else {
                                    car_model[i] = listCarModelList.get(i).getTitle();
                                }
                            }
                            //Set model spinner
                            setSpinner_model(car_model);
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CarModelData> call, Throwable t) {
                dialog.dismiss();
                Log.e("sppiner 2 ", "values= "+t);
            }
        });
    }

    private void setSpinnerValues(String[] listData) {

        adapter_make = new ArrayAdapter(HomeActivity.this, android.R.layout.simple_spinner_item, listData);
        adapter_make.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_make.setAdapter(adapter_make);
        spinner_make.setSelection(spinner_make.getSelectedItemPosition());

        spinner_make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (car_make[i].equalsIgnoreCase("Select Car Make")) {
                    Log.e("sppiner", "item click resume = " + car_make[i] + " position= " + i);
                } else {
                    car_make_id =  carMakeDataList.get(i).getId();
                    Log.e("sppiner", "item click= " + car_make[i] + " position= " +  car_make_id);
                    ConstantMember.setValueInSession(context,"car_make_id",""+car_make_id);
                    apiCallCarModel();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

   public void setSpinner_model(String[] carModelList) {
       adapter_model = new ArrayAdapter(this, android.R.layout.simple_spinner_item, carModelList);
       adapter_model.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       spinner_model.setAdapter(adapter_model);

       spinner_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               if (listCarModelList.get(i).getTitle().equalsIgnoreCase("Select Car Model")) {
                   Log.e("sppiner", "item click= " + car_model[i] + " position= " +i);
               } else {
                   String car_type = listCarModelList.get(i).getCar_type();
                   String car_model_id = ""+listCarModelList.get(i).getId();
                   ConstantMember.setValueInSession(context,"car_model_id",""+car_model_id);
                   ConstantMember.setValueInSession(context,"car_type",""+car_type);
                   editTextCarType.setText(car_type);
               }
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });
   }

    @Override
    public void onClick(View v) {
        if (issubscribed != 0) {
            if (statusCheckGps()) {
                if (validate()) {
                    startActivity(new Intent(HomeActivity.this, InspectionActivity.class));
                }
            }
        } else {
            initSteps();
        }
    }

    private boolean validate() {
        boolean flag = true;
        car_number = "" + editText_carNumber.getText();
        Log.e("valid","car_no="+isValidCarNo(car_number));
        ConstantMember.setValueInSession(context,"car_number",car_number);

        if (car_number.length() == 0) {
            editText_carNumber.setError("Please enter car number");
            flag = false;
        }

        if (car_number.length() > 0) {
            if (!isValidCarNo(car_number)) {
                carValidDialog();
                flag = false;
            }
        }

        if (ConstantMember.getValueFromSession(context, "car_make_id").equals("")) {
            flag = false;
            ConstantMember.showMessage(context, "Please Select Car Make");
        } else if (ConstantMember.getValueFromSession(context, "car_model_id").equals("")) {
            flag = false;
            ConstantMember.showMessage(context, "Please Select Car Model");
        }

        return flag;
    }

    private void carValidDialog() {
        new android.app.AlertDialog.Builder(HomeActivity.this)
                .setTitle("Car Number")
                .setMessage("Please enter car number in"+"\n\n"+ "XX 00 XX 0000 or XX 00 X 0000" +
                        "\n\n"+"E.g. DL 01 AB 1234 or DL 01 A 1234")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public boolean statusCheckGps() {
        boolean flag = true;

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
           flag = false;
        }
        return  flag;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS in disabled, kindly enable with High Accuracy mode")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isValidCarNo(final String car_number) {

        Pattern pattern;
        Matcher matcher;

        final String CAR_NO_PATTERN = "^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z]{2})?(?: [A-Z]{1,2}[ -]*)? [0-9]{4}$";

        pattern = Pattern.compile(CAR_NO_PATTERN);
        matcher = pattern.matcher(car_number);

        return matcher.matches();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showDialog() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    //Code google login
    public void initSteps() {
        FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .build();
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(context), fitnessOptions)) {
            GoogleSignIn.requestPermissions(this,REQUEST_OAUTH_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(context),
                    fitnessOptions);
        } else {
            subscribe();
        }
    }
    public void subscribe() {
        // To create a subscription, invoke the Recording API. As soon as the subscription is
        Fitness.getRecordingClient(context, GoogleSignIn.getLastSignedInAccount(context))
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    issubscribed=1;
                                } else {
                                    Log.e("Step3","fit option "+task.getException());
                                }
                        }
                  });
    }
}
